import numpy as np 
import mindspore.nn as nn
from mindspore import Parameter , context
from mindspore import Tensor,ops
import mindspore as ms 
import random
from mindspore.common.initializer import initializer, Initializer, TruncatedNormal, Zero

#context.set_context(device_target='Ascend')

def tensor2number(input):
    if isinstance(input,Tensor):
        input = input.asnumpy()
        input = input.tolist()
    return input

#Linear
class MyLinear(nn.Cell):
    def __init__(self,input_size,output_size,gain=2**0.5,use_wscale=False,lrmul=1,bias=True):
        super(MyLinear,self).__init__()
        
        self.dense = nn.Dense(input_size,output_size,weight_init="HeNormal",has_bias=bias)
        
    def construct(self,x):
        output = self.dense(x)                            #全连接，x matmul W.T + bias 为了和torch保持一致
        return output

'''Convolution layer , including deconvolution to larger the resolution'''
class MyConv2d(nn.Cell):#pure conv2d  checked!
    def __init__(self,input_channels,output_channels,kernel_size,pad_mode='same',padding=0,gain=2**0.5,use_wscale=False,lrmul=1,bias=True,
        intermediate=None):
        super(MyConv2d,self).__init__()
        
        #he_std = gain * (input_channels*kernel_size**2)**(-0.5)     #He_init
        #self.kernel_size = kernel_size
        ##if use_wscale:
            #init_std = 1.0/lrmul
            #self.w_mul = he_std*lrmul
        #else:
            #init_std = 1.0
            #self.w_mul = lrmul
        #self.weight = Parameter(Tensor(np.random.randn(output_channels,input_channels,kernel_size,kernel_size)*init_std,dtype=ms.float32))
        #if bias :
            #self.bias = Parameter(Tensor(np.zeros(output_channels),dtype=ms.float32))
            #self.b_mul = lrmul
        #else:
            #self.bias = None
        #self.intermediate = intermediate
        #self.conv2d_transpose = ops.Conv2D(out_channel=output_channels,stride=2,kernel_size=3,mode=2)
        
        self.conv2d = nn.Conv2d(input_channels,output_channels,kernel_size=kernel_size,pad_mode=pad_mode,padding=padding,weight_init="HeUniform",has_bias=bias)  
    def construct(self,x):
        #if self.intermediate is None:
        return self.conv2d(x)

        #if self.intermediate is not None:
            #x = 
        #if bias is not None:
            #x = x + bias.view(1,-1,1,1)
            #return self.intermediate(x)

"""Blur Layer"""
class BlurLayer(nn.Cell):#changed!  add x for GRAPHMODE
    def __init__(self,kernel=[1,2,1],normalize=True,flip=False,stride=1):
        super(BlurLayer,self).__init__()
        #kernel = [1,2,1]
        #kernel = np.asarray(kernel)
        #kernel = kernel[:,None] * kernel[None,:]
        #kernel = kernel[None,None]
        self.kernel = Tensor(([[[[1, 2, 1],
         [2, 4, 2],
         [1, 2, 1]]]]),dtype=ms.float32)
        if normalize:
            self.kernel = self.kernel / (ops.ReduceMean()(self.kernel) * 9)         #ms没有 tensor.sum()
        if flip:
            #self.kernel = np.flip(kernel,(2,3))
            self.kernel = ops.Transpose()(self.kernel,(0,1,3,2))
        #self.register_buffer('kernel',kernel)            torch中语句，还未弄清作用
        #kernel = Tensor(kernel,dtype=ms.float32)

        self.stride = stride
        #self.kernel = kernel
        #self.kernel = Tensor(self.kernel,dtype=ms.float32)
        #self.kernel = ops.BroadcastTo((x.shape[1],self.kernel.shape[1],self.kernel.shape[2],self.kernel.shape[3]))(self.kernel)
        #self.conv2d = ops.Conv2D(x.shape[1],kernel_size=3,stride=self.stride,pad_mode='pad',pad=1,group=x.shape[1])
    def construct(self,x):
        #self.kernel = np.broadcast_to(self.kernel,(x.shape[1],self.kernel.shape[1],self.kernel.shape[2],self.kernel.shape[3]))
        kernel = ops.BroadcastTo((x.shape[1],self.kernel.shape[1],self.kernel.shape[2],self.kernel.shape[3]))(self.kernel)
        out = ops.Conv2D(x.shape[1],3,1,'pad',1,self.stride,1,x.shape[1])(x,kernel)
        return out

#Noise
class NoiseLayer(nn.Cell):#checked!                                                  #加入噪声层，输出为加入噪声后的输入
    def __init__(self,channels):
        super(NoiseLayer,self).__init__()
        self.weight = Parameter(Tensor(np.zeros([1,channels,1,1]),dtype=ms.float32))
        #self.noise = None
        #self.shape = (batch,channels,size,size)
        #self.broadcast = ops.BroadcastTo(self.shape)
        #self.noise = Tensor(np.random.randn(x.shape[0],1,x.shape[2],x.shape[3]),dtype=ms.float32)
        self.noise = Tensor(np.random.randn(1,1,1,1),dtype=ms.float32)
    def construct(self,x,noise=None):
        if noise is None:
            noise_out = ops.BroadcastTo((x.shape[0],1,x.shape[2],x.shape[3]))(self.noise)
        #elif noise is None:
    
        #weight = self.broadcast(self.weight)
            #x = x + self.weight * self.noise
            x = ops.Add()(x,ops.Mul()(self.weight,noise_out))
            return x
        else:
            return ops.ZerosLike()(x)


class StyleMod(nn.Cell):                                                        #styleMod,输入为mapping后的W，输出为AdaIN后的x
    def __init__(self,latent_size,channels,use_wscale=False):
        super(StyleMod,self).__init__()
        #self.lin = MyLinear(latent_size,2*channels,gain=1,use_wscale=use_wscale)
        self.channels = channels
    def construct(self,x,latent):                                               #这块形状一直没咋搞懂
        #style = self.lin(latent)                                                #style[batch+size,2channels]
        style = MyLinear(latent.shape[1],2*self.channels,gain=1)(latent)
        shape = [-1,2,x.shape[1]] + (x.ndim-2) * [1]                            #x.ndim = 4
        style = style.view(tuple(shape))
        x = x * (style[:,0] + 1.) + style[:, 1]
        return x 

def mean_and_var(tensor,axes):
    y = ops.Cast()(tensor,ms.float32)
    mean = ops.ReduceMean(True)(y,axes)
    var = ops.ReduceMean(True)(ops.Square()(y-mean),axis=(2,3))
    return mean,var
    
def InstanceNorm2d(input):#checked!
    eps=Tensor(1e-5,dtype=ms.float32)
    mean,var = mean_and_var(input,axes=(2,3))
    #print(mean.shape)
    #print(var.shape)
    scale = initializer(TruncatedNormal(),[input.shape[1]],ms.float32).to_tensor()
    scale = ops.Add()(scale,1)
    scale = ops.Reshape()(scale,(1,input.shape[1],1,1))
    #print(scale.shape)
    offset = initializer(Zero(),[1],ms.float32).to_tensor()
    out = ops.Mul()(scale,ops.Mul()(input-mean,ops.Rsqrt()(var + eps))) + offset
    #input = input.asnumpy()
    #std = np.std(input)
    #mean = input.mean()
    #y = (input - mean) / (std**2 + eps)
    #y = Tensor(y,dtype=ms.float32)
    return out

#AdaIN
class AdaIN(nn.Cell):#checked!
    def __init__(self,in_channel,style_dim):
        super(AdaIN,self).__init__()
        #self.norm = nn.InstanceNorm2d(in_channel)
        self.style = MyLinear(style_dim,in_channel * 2)

        self.style.dense.bias[:in_channel] = 1
        self.style.dense.bias[in_channel:] = 0
        
        self.reshape = ops.Reshape()
        self.split = ops.Split(1,2)
    def construct(self,input,style):
        #print("self.style.dense.bias",self.style.dense.bias[:512].shape)
        style = self.style(style)
        #style = ops.ExpandDims()(style,2)
        #style = ops.ExpandDims()(style,3)
        #style = self.reshape(style,(style.shape[0],style.shape[1],1,1))
        gamma,beta = self.split(style)#[batch_size,in_channel,1,1]
        out = InstanceNorm2d(input)       #[batch_size,in_channel,height,width]
        gamma = self.reshape(gamma,(gamma.shape[0],gamma.shape[1],1,1))
        beta = self.reshape(beta,(beta.shape[0],beta.shape[1],1,1))
        out = gamma * out + beta
        #out = ops.Add()(ops.Mul()(gamma,out),beta)
        return out 
 #constant input:
class Constant_Input(nn.Cell):
    def __init__(self, channel,size=4):
        super(Constant_Input,self).__init__()

        self.input = Tensor(np.random.randn(1,channel,size,size),dtype=ms.float32)
    
    def construct(self,input):
        batch_size = input.shape[0]
        out = ops.repeat_elements(self.input,batch_size,0)
        return out

'''Upscale'''
def upscale2d(x,factor=2,gain=1):                   #An implementation to double the resolution , also called h and w
    assert x.ndim == 4
    if gain != 1:
        x = x * gain
    if factor != 1:
        shape = x.shape
        x = x.view(shape[0],shape[1],shape[2],1,shape[3],1)
        #x = x.asnumpy()
        #x = np.broadcast_to(x,(shape[0],shape[1],shape[2],factor,shape[3],factor))#骚操作
        x = ops.BroadcastTo((shape[0],shape[1],shape[2],factor,shape[3],factor))(x)
        #x = np.ravel(x).view((shape[0],shape[1],shape[2]*factor,shape[3]*factor))
        #x = Tensor(x,ms.float32)
        x = x.view(shape[0],shape[1],shape[2]*factor,shape[3]*factor)
    return x

class Upscale2d(nn.Cell):
    def __init__(self,factor=2,gain=1):
        super(Upscale2d,self).__init__()
        assert isinstance(factor,int) and factor >= 1
        self.gain = gain
        self.factor = factor
    def construct(self,x):
        return upscale2d(x,self.factor,self.gain)

#FusedUpsample
class FusedUpsample(nn.Cell):
    def __init__(self,in_channel,out_channel,kernel_size,padding):
        super(FusedUpsample,self).__init__()

        weight = Tensor(np.random.randn(in_channel,out_channel,kernel_size,kernel_size),dtype=ms.float32)

        fan_in = Tensor(in_channel * kernel_size * kernel_size,ms.float32)
        self.multiplier = ops.Sqrt()(2 / fan_in)
        #self.weight = Parameter(weight)
        
        self.pad = padding
        # self.Pad = ops.Pad(((0,0),(0,0),(1,1),(1,1)))
        self.conv2d_transpose = nn.Conv2dTranspose(in_channel,out_channel,kernel_size,stride=2,pad_mode="pad",padding=(1,0,1,0),has_bias=True,weight_init='HeUniform')
    def construct(self,input):
        #weight = self.Pad(self.weight * self.multiplier)
        '''weight = (                                                      #Fuse 
            weight[:, :, 1:, 1:]
            + weight[:, :, :-1, 1:]
            + weight[:, :, 1:, :-1]
            + weight[:, :, :-1, :-1]
        ) / 4   '''      
        
        out = self.conv2d_transpose(input) 
        return out 

#FusedDownsample
class FusedDownsample(nn.Cell):#checked!
    def __init__(self,in_channel,out_channel,kernel_size,padding):
        super(FusedDownsample,self).__init__()

        weight = Tensor(np.random.randn(out_channel,in_channel,kernel_size,kernel_size),dtype=ms.float32)
        bias = Tensor(np.zeros(out_channel),ms.float32)

        fan_in = Tensor(in_channel * kernel_size * kernel_size,ms.float32)
        self.multiplier = ops.Sqrt()(2 / fan_in)

        #self.weight = Parameter(weight)
        #self.bias = Parameter(bias)
        
        self.Pad = ops.Pad(((0,0),(0,0),(1,1),(1,1)))
        self.pad = padding
        #self.conv2d = ops.Conv2D(out_channel,kernel_size,pad_mode='pad',pad=self.pad,stride=2)
        self.conv2d = nn.Conv2d(in_channel,out_channel,kernel_size,stride=2,pad_mode='pad',padding=padding,has_bias=True,weight_init='HeUniform')
    def construct(self,input):
        #weight = self.Pad(self.weight * self.multiplier)
        #weight = (
            #weight[:, :, 1:, 1:]
            #+ weight[:, :, :-1, 1:]
            #+ weight[:, :, 1:, :-1]
            #+ weight[:, :, :-1, :-1]
        #) / 4

        out = self.conv2d(input) 
        return out

class PixelNormLayer(nn.Cell):#checked!
    def __init__(self,epsilon=1e-8):
        super(PixelNormLayer,self).__init__()
        self.epsilon = Tensor(epsilon,dtype=ms.float32)
    
    def construct(self,x):
        return x * ops.Rsqrt()(x.mean(axis=1,keep_dims=True) + self.epsilon)

#ConvBlock
class ConvBlock(nn.Cell):
    def __init__(
        self,
        in_channel,
        out_channel,
        kernel_size,
        padding,
        kernel_size2=None,
        pad_mode2 = 'same',
        padding2 = None,
        downsample=False,
        fused=False,
    ):
        super(ConvBlock,self).__init__()
        pad1 = padding
        pad2 = padding

        if padding2 is not None:
            pad2 = padding2
        
        kernel1 = kernel_size
        kernel2 = kernel_size

        if kernel_size2 is not None:
            kernel2 = kernel_size2
        
        self.conv1 = nn.SequentialCell([
            MyConv2d(in_channel,out_channel,kernel1),
            nn.LeakyReLU(0.2)
        ])

        if downsample:
            if fused:
                self.conv2 = nn.SequentialCell([
                    BlurLayer(),
                    FusedDownsample(out_channel,out_channel,kernel2,padding=pad2),
                    nn.LeakyReLU(0.2)
                ])
            else:
                self.conv2 = nn.SequentialCell([
                    BlurLayer(),
                    MyConv2d(out_channel,out_channel,kernel2),
                    nn.AvgPool2d(2,2),
                    nn.LeakyReLU(0.2)
                ])
        
        else:
            self.conv2 = nn.SequentialCell([
                MyConv2d(out_channel,out_channel,kernel2,pad_mode=pad_mode2,padding=padding2),
                nn.LeakyReLU(0.2)
            ])
    
    def construct(self,input):
        out = self.conv1(input)
        out = self.conv2(out)

        return out

#styleconvblock
class StyleConvBlock(nn.Cell):
    def __init__(
        self,
        in_channel,
        out_channel,
        kernel_size=3,
        padding=1,
        style_dim=512,
        initial=False,
        upsample=False,
        fused=False
    ):
        super(StyleConvBlock,self).__init__()
        if initial:
            self.conv1 = Constant_Input(in_channel)
        
        else:
            if upsample:
                if fused:
                    self.conv1 = nn.SequentialCell([
                        FusedUpsample(in_channel,out_channel,kernel_size,padding=padding),
                        BlurLayer(),
                    ])
                
                else:
                    '''self.conv1 = nn.SequentialCell([
                        #MyConv2d(in_channel,out_channel,kernel_size),
                        Upscale2d()
                        BlurLayer()
                    ])'''
                    self.conv1 = nn.SequentialCell([
                        FusedUpsample(in_channel,out_channel,kernel_size,padding=padding),
                        BlurLayer(),
                    ])
            else:
                self.conv1 = MyConv2d(in_channel,out_channel,kernel_size)
            
        #self.noise1 = NoiseLayer(out_channel)
        self.adain1 = AdaIN(out_channel,style_dim)
        self.lrelu1 = nn.LeakyReLU(0.2)

        self.conv2 = MyConv2d(out_channel,out_channel,kernel_size)
        #self.noise2 = NoiseLayer(out_channel)
        self.adain2 = AdaIN(out_channel,style_dim)
        self.lrelu2 = nn.LeakyReLU(0.2)
    def construct(self,input,latent,noise=None):
        out = self.conv1(input)
        #out = self.noise1(out,noise)
        #print("noise1后:",out.shape)
        out = self.adain1(out,latent)
        out = self.lrelu1(out)
        out = self.conv2(out)
        #out = self.noise2(out,noise)
        out = self.adain2(out,latent)
        out = self.lrelu2(out)
        return out

class Generator(nn.Cell):#brief changed  x=Tensor...
    def __init__(self,code_dim,fused=False):
        super(Generator,self).__init__()

        self.progression = nn.SequentialCell([
            StyleConvBlock(512,512,3,1,initial=True),           #恒输入，输出4
            StyleConvBlock(512,512,3,1,upsample=True),          #8
            StyleConvBlock(512,512,3,1,upsample=True),          #16
            StyleConvBlock(512,512,3,1,upsample=True),          #32
            StyleConvBlock(512,256,3,1,upsample=True),          #64
            StyleConvBlock(256,128,3,1,upsample=True,fused=True),          #128
            StyleConvBlock(128,64,3,1,upsample=True,fused=True),          #256
            StyleConvBlock(64,32,3,1,upsample=True,fused=True),          #512
            StyleConvBlock(32,16,3,1,upsample=True,fused=True)       #1024
        ]   
        )

        self.to_rgb = nn.SequentialCell([
            MyConv2d(512,3,1),
            MyConv2d(512,3,1),
            MyConv2d(512,3,1),
            MyConv2d(512,3,1),
            MyConv2d(256,3,1),
            MyConv2d(128,3,1),
            MyConv2d(64,3,1),
            MyConv2d(32,3,1),
            MyConv2d(16,3,1)
        ])
        self.upscale = Upscale2d()
        #self.initial_input = Tensor(np.zeros((latent[0].shape[0],512,4,4)),dtype=ms.float32)
    def construct(self,latent,step=0,alpha=-1,noise=None,mixing_range=(-1,-1)):
        step = tensor2number(step)
        alpha = tensor2number(alpha)
        #out = noise[0]
        out = Tensor(np.zeros((latent[0].shape[0],512,4,4)),dtype=ms.float32)
        #out = self.initial_input
        if len(latent) < 2:#batch_size<2
            inject_index = [len(self.progression) + 1]

        else:
            inject_index = sorted(random.sample(list(range(step)),len(latent)-1))        #相当创建一个step到batch_size的列表
        
        crossover = 0

        for i , (conv,to_rgb) in enumerate(zip(self.progression,self.to_rgb)):
            if mixing_range == (-1,-1):
                if crossover < len(inject_index) and i > inject_index[crossover]:
                    crossover = min(crossover+1,len(latent))
                latent_step = latent[crossover]                                             #取出第crossover个 W

            else:
                if mixing_range[0] <= i <= mixing_range[1]:
                    latent_step = latent[1]
                else:
                    latent_step = latent[0]
            
            if i>0 and step > 0 :
                out_prev = out
            
            out = conv(out,latent_step)
            
            
            if i == step:                                                                   #什么时候相等？
                out = to_rgb(out)

                if i > 0 and 0 <= alpha < 1:
                    skip_rgb = self.to_rgb[i-1](out_prev)
                    skip_rgb = self.upscale(skip_rgb)
                    out = (1 - alpha) * skip_rgb + alpha * out                              #momentum?

                break
        return out 


class StyleGenerator(nn.Cell):
    def __init__(self,code_dim=512,n_mlp=8):
        super(StyleGenerator,self).__init__()

        self.generator = Generator(code_dim)

        #layers = nn.SequentialCell([PixelNormLayer()])
        layers = [PixelNormLayer()]
        for i in range(n_mlp):                                          #mapping network
            layers.append(MyLinear(code_dim,code_dim))
            layers.append(nn.LeakyReLU(0.2))
        
        self.style = nn.SequentialCell(layers)                        #style is W or latent

    def construct(self,
        input,
        step=0,
        alpha=-1,
        noise=None,
        mean_style=None,
        style_weight=0,
        mixing_range=(-1,-1),
    ):
        step = tensor2number(step)
        alpha = tensor2number(alpha)
        styles = []                                                     
        if type(input) not in (list,tuple):
            input = [input]
        
        for i in input :
            styles.append(self.style(i))

        batch = input[0].shape[0]

        '''if noise is None:
            noise = []

            for i in range (step+1):
                size = 4 * 2 ** i                                   #what size?
                if i == 0:
                    noise_temp = Tensor(np.random.randn(batch,1,size,size),dtype=ms.float32)
                #noise_temp = Tensor(np.random.randn(batch,1,1,1),dtype=ms.float32)
                    noise.append(noise_temp)
                else:
                    noise_temp = Tensor(np.random.randn(batch,1,size,size),dtype=ms.float32)
        '''
        if mean_style is not None:
            styles_norm = []

            for style in styles:#all tensor
                styles_norm.append(mean_style + style_weight * (style - mean_style))
            
            styles = styles_norm
        #noise = Tensor(noise,dtype=ms.float32)
        #styles = Tensor(styles,dtype=ms.float32)
        return self.generator(styles,step=step,alpha=alpha,noise=noise,mixing_range=mixing_range)
    
    def mean_style(self,input):
        style = self.style(input).mean(0,keepdim=True)

        return style


class Discriminator(nn.Cell):
    def __init__(self,fused=True,from_rgb_activation=False):
        super(Discriminator,self).__init__()

        self.progression = nn.SequentialCell([
            ConvBlock(16,32,3,1,downsample=True,fused=fused),   #512
            ConvBlock(32,64,3,1,downsample=True,fused=fused),   #256
            ConvBlock(64,128,3,1,downsample=True,fused=fused),  #128
            ConvBlock(128,256,3,1,downsample=True,fused=fused), #64
            ConvBlock(256,512,3,1,downsample=True),             #32
            ConvBlock(512,512,3,1,downsample=True),             #16
            ConvBlock(512,512,3,1,downsample=True),             #8
            ConvBlock(512,512,3,1,downsample=True),             #4
            ConvBlock(513,512,3,1,4,pad_mode2='pad',padding2=0)
        ])

        def make_from_rgb(out_channel):
            if from_rgb_activation:
                return nn.SequentialCell([MyConv2d(3,out_channel,1),nn.LeakyReLU(0.2)])

            else:
                return MyConv2d(3,out_channel,1)
        self.from_rgb = nn.CellList(
            [
                make_from_rgb(16),
                make_from_rgb(32),
                make_from_rgb(64),
                make_from_rgb(128),
                make_from_rgb(256),
                make_from_rgb(512),
                make_from_rgb(512),
                make_from_rgb(512),
                make_from_rgb(512),
            ]
        )
 
        self.n_layer = len(self.progression)

        self.linear = MyLinear(512,1)
        self.avfpool2d = ops.AvgPool(2,strides=2)
        #self.ToolVector = Tensor(np.zeros((batch_size,1,4,4)))
    def construct(self,input,step,alpha=-1):
        step = tensor2number(step)
        alpha = tensor2number(alpha)
        #step = self.step
        for i in range(step,-1,-1):
            
            index = self.n_layer - i - 1
            if i == step:
                out = self.from_rgb[index](input)
            
            if i == 0:
                '''out_std = Tensor(ops.Sqrt()((np.var(out)+ 1e-8).asnumpy()),ms.float32)
                mean_std = out_std.mean()
                #mean_std = np.broadcast_to(out.shape[0],1,4,4)
                mean_std = ops.BroadcastTo((out.shape[0],1,4,4))(mean_std)
                #out = np.concatenate((out,mean_std),axis=1)
                out = ops.Concat(1)((out,mean_std))'''
                #out_std = Tensor(ops.Sqrt()((np.var(out)+ 1e-8).asnumpy()),ms.float32)
                #out = out.asnumpy()
                #var = np.var(out.asnumpy(),axis=0,keepdims=True) + 1e-8
                mean = ops.ReduceMean(True)(out,axis=0)
                var = ops.ReduceMean(True)(ops.Square()(out-mean),axis=0)
                #var = Tensor(var,dtype = ms.float32)
                out_std = ops.Sqrt()(var)
                mean_std = ops.ReduceMean()(out_std)

                #mean_std = np.broadcast_to(out.shape[0],1,4,4)
                mean_std = ops.BroadcastTo((out.shape[0],1,4,4))(mean_std)
                #mean_std = mean_std.expand_as(self.ToolVector)
                #out = np.concatenate((out,mean_std),axis=1)
                out = ops.Concat(1)((out,mean_std))
            out = self.progression[index](out)
            
            if i > 0:
                if i == step and 0 <= alpha < 1:
                    skip_rgb = self.avfpool2d(input)
                    skip_rgb = self.from_rgb[index+1](skip_rgb)

                    out = (1-alpha) * skip_rgb + alpha * out
            
        #out = np.squeeze(np.squeeze(out,2),2)
        out = ops.Squeeze(2)(out)
        out = ops.Squeeze(2)(out)
        out = self.linear(out)

        return out 
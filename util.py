#import mindspore.numpy as np 
import numpy as np
from mindspore import Tensor,ops
import mindspore as ms
import math
from PIL import Image


def make_grid(
    tensor,                 #输入
    nrow,                   #多少行
    #padding,
    normalize,              #是否正则化
    value_range=(-1,1),#: tuple:(min,max),#正则化范围
    pad_value = 0
):
    print("running1")                                                   #输入是list,元素为tensor
    if not isinstance(tensor,Tensor) and not isinstance(tensor,list):
        raise TypeError(f'tensor or list of tensors expected, got {type(tensor)}')
    
    if isinstance(tensor,Tensor):
        tensor = [tensor]

    #if tensor.ndim == 2:#single img
        #tensor = np.expand_dims(tensor,0)
        #tensor = ops.ExpandDims()(tensor,0)
    #if tensor.ndim == 3:
        #if tensor.shape[0] == 1:
            #tensor = np.concatenate((tensor,tensor,tensor),0)
            #tensor = ops.Concat(0)((tensor,tensor,tensor))
        #tensor = np.expand_dims(tensor,0)
        #tensor = ops.ExpandDims()(tensor,0)
    #if tensor.ndim == 4 and tensor.shape[1] == 1:
        #tensor = np.concatenate((tensor,tensor,tensor),1)
        #tensor = ops.Concat(1)((tensor,tensor,tensor))
    #print("running3")
    if normalize :
        tensor_1 = tensor
        if value_range is not None:
            assert isinstance(value_range,tuple)
        
        def norm_ip(img,low,high):
            for i in range(len(img)):
                img[i] = np.clip(img[i].asnumpy(),low,high)
                img[i] = ops.Div(img[i]-low,max(high-low,1e-5))
                img[i] = Tensor(img[i],dtype=ms.float32)
        def norm_range(t,value_range):
            if value_range is not None:
                norm_ip(t,value_range[0],value_range[1])
            
            else:
                print("Please examplify the value range!")
    print("running4")
    #if tensor.shape[0] == 1:
        #return np.expand_dims(tensor,0)
        #return ops.ExpandDims()(tensor,0)
        #return ops.Squeeze(0)(tensor)
    
    #nmaps = tensor.shape[0]
    nmaps = len(tensor) * tensor[0].shape[0]
    xmaps = min(nrow,nmaps)                             #横轴方向图片数
    ymaps = int(math.ceil(float(nmaps) / xmaps))        #纵轴方向图片数
    height,width = tensor[0].shape[2],tensor[0].shape[3]#每个图片的大小

    num_channels = tensor[0].shape[1]
    grid = ops.Fill()(ms.float32,(num_channels,height*ymaps,width*xmaps),pad_value)
    grid = grid.asnumpy()
    print("running5")
    for y in range(ymaps):
        for x in range(xmaps):
            
            #np.take_along_axis(np.take_along_axis(grid,[i for i in range(y*height,(y+1)*height),1]),2,[j for j in range(x*width,(x+1)*width,1)])

            grid[:,height*y:height*(y+1),width*x:width*(x+1)] = tensor[x][y].asnumpy()
    grid = Tensor(grid,ms.float32)
    return grid

def save_image(
    tensor,
    fp,
    #format,
    **kwargs
):
    grid = make_grid(tensor,**kwargs).asnumpy()
    
    ndarr = np.clip(255*grid + 0.5,0,255)
    ndarr = np.moveaxis(ndarr,0,-1)
    
    im = Image.fromarray(np.uint8(ndarr))
    #im.save(fp,format=format)
    im.save(fp)


if __name__ == "__main__":
    a = Tensor(np.random.randn(1,3,64,64),dtype = ms.float32)
    b = Tensor(np.random.randn(1,3,64,64),dtype = ms.float32)
    c = Tensor(np.random.randn(1,3,64,64),dtype = ms.float32)
    img = [a,b,c]
    save_image(
        img,
        f'{str(1).zfill(6)}.png',
        nrow=10,
        normalize=True
    )
import argparse
import random
import math
from mindspore.communication.management import init
from mindspore.communication.management import get_group_size
from mindspore.common import set_seed

import os 

import moxing as mox

from tqdm import tqdm
import numpy as np 
from PIL import Image 

from model_version1 import *

import mindspore 
from mindspore import nn,ParameterTuple,context
import mindspore.ops.operations as P
import mindspore.ops.functional as F
import mindspore.ops as ops
from mindspore import DatasetHelper
import mindspore.dataset as ds 
from mindspore.parallel._utils import (_get_device_num, _get_gradients_mean,
                                       _get_parallel_mode)
from mindspore.context import ParallelMode
from mindspore.nn.wrap.grad_reducer import DistributedGradReducer
import mindspore.dataset.vision.c_transforms as CV
import mindspore.dataset.vision.py_transforms as PV 
from mindspore.dataset.transforms.py_transforms import Compose

from util import save_image
from dataset import MultiResolutionDataset

context.set_context(device_target="Ascend")
context.set_context(mode=context.PYNATIVE_MODE)

#context.set_context(mode=context.GRAPH_MODE)

#context.set_auto_parallel_context(device_num=1, gradients_mean=True, parallel_mode='data_parallel')
#context.set_context(device_target='Ascend')
#init('HCCL')
#context.set_auto_parallel_context(device_num=1, gradients_mean=True, parallel_mode='data_parallel')

set_seed(1)


def type_Transform(input):
    input = input.asnumpy()
    return Tensor(input,dtype=ms.float32)

def requires_grad(model,flag=True):                     #设置梯度标签
    for name,para in model.parameters_and_names():
        para.requires_grad = flag

def accumulate(model1,model2,decay=0.999):              #不太懂，应该是并行处理时需要的参数
    par1 = model1.parameters_dict()
    par2 = model2.parameters_dict()

    for k in par1.keys():
        par1[k] = par1[k] * decay + (1-decay)*par2[k]

def create_dataset(dataset,batch_size,transform,buffer_size=100,repeat_size=1):       #取出数据就行
    #dataset.resolution = image_size
    #data = CelebADataset(dataset_dir,num_parallel_workers=num_parallel_workers)
    data = ds.GeneratorDataset(dataset,column_names=["images","gbs"])
    data = data.map(transform)
    #data = data.shuffle(buffer_size=buffer_size)
    data = data.batch(batch_size,True)
    data = data.repeat(repeat_size)
    return data

def data_sampler(path,batch_size,resolution):
    dataset_0 = MultiResolutionDataset(path,resolution=resolution)
    transform = Compose(                                                 #Composes several transforms together.
        [   PV.ToPIL(),
            PV.RandomHorizontalFlip(),
            PV.ToTensor(),
            PV.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
        ]
    )
    dataset = create_dataset(dataset_0,batch_size,transform=transform)
    return iter(dataset)

def adjust_lr(optimizer,lr):                            #mindspore的Optimizer类没有添加参数的方法，建立一个字典来保存参数
    mult = optimizer_param_groups.get('mult',1)
    lr = lr * mult
    #optimizer._build_single_lr(lr * mult,'lr')      #修改optimizer的learning_rate
    optimizer.learning_rate.set_data(lr*mult)
def Array2Tensor(array_in):
    #make np.ndarray 2 Tensor in ms
    return Tensor(array_in,dtype=ms.float32)

def save_ckpt(net,ckpt_file):
    mindspore.save_checkpoint(net,ckpt_file)

def load_ckpt(ckpt_file,net):
    param_dict = ms.load_checkpoint(ckpt_file)
    ms.load_param_into_net(net,param_dict)


class DisWithLossCell(nn.Cell):
    def __init__(self,netG,netD,loss_fn='wgan-gp',auto_prefix=False):
        super(DisWithLossCell,self).__init__(auto_prefix=False)
        self.netG = netG
        self.netD = netD
        self.loss_fn = loss_fn
        self.grad = ops.GradOperation()
        self.add_op = P.Add()
        self.mul = P.Mul()
        self.sub = P.Sub()
        self.pow = P.Pow()
    def construct(self,real_image,step,alpha,mixing=False):
        b_size = real_image.shape[0]
        #eps = Array2Tensor(np.random.rand(b_size,1,1,1))
        
        #if self.loss_fn == 'wgan-gp':
        real_scores = self.netD(real_image,step,alpha)
        real_predict = ops.Softplus()(-real_scores).mean()
        #real_predict = real_predict.mean() - 0.001 * self.pow(real_predict,2).mean()         #啊，这就是惩罚项啊  fake-real+epsilon*p=fake-(real-eps*p)
        #grad_real = self.grad(self.netD)(real_image,step,alpha)
        #grad_penalty = (
            #self.pow((nn.Norm(axis=1)(grad_real.view(grad_real.shape[0],-1))) , 2)
        #).mean()
        #grad_penalty = 10 / 2 * grad_penalty
        #gen_in1,gen_in2 = np.split(np.random.randn(2,b_size,512),2,axis=0)
        gen_in1 = np.random.randn(b_size,512)
        gen_in1 = Array2Tensor(gen_in1)
        #gen_in2 = Array2Tensor(gen_in2.squeeze(0))
        fake_image = self.netG(gen_in1,step,alpha)                               #生成图片
        fake_predict = self.netD(fake_image,step,alpha)                      #判别生成图片
        fake_predict = ops.Softplus()(fake_predict).mean()
        
        fake_predict = fake_predict.mean()

        #x_hat = eps * real_image + (1 - eps) * fake_image
        #x_hat = self.mul(eps,real_image)
        #x_hat_1 = self.sub(1,eps)
        #x_hat_2 = self.mul(self.sub(1,eps),fake_image)
        

        #loss_D = fake_predict - real_predict + grad_penalty
        loss_D = real_predict + fake_predict 
        return loss_D



class GenWithLossCell(nn.Cell):
    def __init__(self,netG,netD,loss_fn='wgan-gp',auto_prefix=False):
        super(GenWithLossCell,self).__init__(auto_prefix=auto_prefix)
        self.loss_fn = loss_fn
        self.netG = netG
        self.netD = netD

    def construct(self,input,step,alpha):
        fake_image = self.netG(input,step,alpha)
        predict = self.netD(fake_image,step,alpha)

        #loss_G = -predict.mean()
        loss_G = ops.Softplus()(-predict).mean()
        return loss_G

   
    
class TrainOneStepD(nn.Cell):
    def __init__(self,netD,optimizerD,sens=1):
        super(TrainOneStepD,self).__init__()

        self.netD = netD
        self.netD.set_grad()
        self.netD.set_train()

        self.optimizer = optimizerD
        
        self.grad = ops.GradOperation(get_by_list=True,sens_param=True)
        self.sens = sens
        #self.weights = ParameterTuple(netD.trainable_params())
        self.weights = optimizerD.parameters

        self.reducer_flag = False
        self.grad_reducer = F.identity
        self.parallel_mode = context.get_auto_parallel_context("parallel_mode")
        if self.parallel_mode in [ParallelMode.DATA_PARALLEL,ParallelMode.HYBRID_PARALLEL]:
            self.reducer_flag = True
        if self.reducer_flag:
            mean = context.get_auto_parallel_context("gradients_mean")
            if auto_parallel_context()._get_device_num_is_set():
                degree = context.get_auto_parallel_context("device_num")
            else:
                degree = get_group_size()
            self.grad_reducer = nn.DistributedGradReducer(optimizerD.parameters,mean.degree)
    
    def trainD(self,real_image,loss,loss_net,grad,grad_reducer,optimizer,weights,step,alpha):
        sens = P.Fill()(P.DType()(loss),P.Shape()(loss),self.sens)
        grads = grad(loss_net,weights)(real_image,step,alpha,sens)
        grads = grad_reducer(grads)
        return F.depend(loss,optimizer(grads))

    def construct(self,real_image,step,alpha):
        loss_D = self.netD(real_image,step,alpha)
        d_out = self.trainD(real_image,loss_D,self.netD,self.grad,self.grad_reducer,self.optimizer,self.weights,step,alpha)

        return d_out


class TrainOneStepG(nn.Cell):
    def __init__(self,netG,optimizerG,sens=1):
        super(TrainOneStepG,self).__init__()

        self.netG = netG
        self.netG.set_grad()
        self.netG.set_train()

        self.optimizer = optimizerG
        
        self.grad = ops.GradOperation(get_by_list=True,sens_param=True)
        self.sens = sens
        #self.weights = ParameterTuple(netG.trainable_params())
        self.weights = optimizerG.parameters

        self.reducer_flag = False
        self.grad_reducer = F.identity
        self.parallel_mode = context.get_auto_parallel_context("parallel_mode")
        if self.parallel_mode in [ParallelMode.DATA_PARALLEL,ParallelMode.HYBRID_PARALLEL]:
            self.reducer_flag = True
        if self.reducer_flag:
            mean = context.get_auto_parallel_context("gradients_mean")
            if auto_parallel_context()._get_device_num_is_set():
                degree = context.get_auto_parallel_context("device_num")
            else:
                degree = get_group_size()
            self.grad_reducer = nn.DistributedGradReducer(optimizerG.parameters,mean.degree)
    
    def trainG(self,latent_code,loss,loss_net,grad,grad_reducer,optimizer,weights,step,alpha):
        sens = P.Fill()(P.DType()(loss),P.Shape()(loss),self.sens)
        grads = grad(loss_net,weights)(latent_code,step,alpha,sens)
        grads = grad_reducer(grads)
        return F.depend(loss,optimizer(grads))

    def construct(self,latent_code,step,alpha):
        loss_G = self.netG(latent_code,step,alpha)
        g_out = self.trainG(latent_code,loss_G,self.netG,self.grad,self.grad_reducer,self.optimizer,self.weights,step,alpha)

        return g_out

        



def train(args,generator,discriminator):
    #some inital configuration
    step = int(math.log2(args.init_size)) - 2 
    step += 1
    resolution = 4 * 2 ** step
    data_loader = data_sampler(local_data_path,batch_size,resolution)


    adjust_lr(g_optimizer, args.lr.get(resolution, 0.001))  #g_optimizer也没定义啊？,用的时候有
    adjust_lr(d_optimizer, args.lr.get(resolution, 0.001))
    pbar = tqdm(range(3_000_000))


    disc_loss_value = 0
    gen_loss_value = 0
    #grad_loss_value = 0

    alpha = 0
    used_sample = 0

    max_step = int(math.log2(args.max_size)) - 2
    final_progress = False
    #train_loop:
    for i in pbar:
        print("第{}代训练开始进行！".format(i))
        alpha = min(1, 1 / args.phase * (used_sample + 1))  #phase：每次训练用的sample数，是对应step吗？
        #Judge if it's the last step
        if  (resolution == args.init_size and args.ckpt is None) or final_progress:
        #if  resolution == args.init_size and args.ckpt is None:
        #if  resolution == args.init_size :
            alpha = 1
            #保存模型
            
        if used_sample > args.phase :
            used_sample = 0
            step += 1

            if step > max_step:
                step = max_step
                final_progress = True
                ckpt_step = step + 1 
            else :
                alpha = 0
                ckpt_step = step
                
                
            resolution = 4 * 2 ** step 
            data_loader = data_sampler(local_data_path,batch_size,resolution)
            #保存模型
            save_ckpt(generator,f'checkpoint/generator_train_step-{ckpt_step}.ckpt')
            save_ckpt(discriminator,f'checkpoint/disriminator_train_step-{ckpt_step}.ckpt')
            adjust_lr(g_optimizer, args.lr.get(resolution, 0.001))              #改变学习率，根据分辨率
            adjust_lr(d_optimizer, args.lr.get(resolution, 0.001))
        try:
            real_image = next(data_loader)[0]
        except(OSError,StopIteration):
            data_loader = data_sampler(local_data_path,batch_size,resolution)
            real_image = next(data_loader)[0]
        
        used_sample += real_image.shape[0]

        #b_size = real_image.shape[0]
        
        #主要训练步骤，求得损失函数
        

        #loss_D = disc_with_loss(real_image,Tensor(step),Tensor(alpha,dtype=ms.float32))
        #print("loss_D:{}".format(loss_D))
        disc_loss_value = train_one_stepD(real_image,Tensor(step),Tensor(alpha,dtype=ms.float32))
        
        #disc_loss_value,gen_loss_value = train_one_step(real_image,latent_code,Tensor(step),Tensor(alpha,dtype=ms.float32))
        
        gen_loss_value = train_one_stepG(latent_code,Tensor(step),Tensor(alpha,dtype=ms.float32))

        #参数给g_running，用于生成图片
        #accumulate(g_running,generator)
        #显示训练过程信息
        if (i+1)%500 == 0 :                                                             #100代保存个图

            gen_i,gen_j = args.gen_sample.get(resolution,(10,5))
            images = []
            #with torch.no_grad()
            for _ in range(gen_i):
                images.append(
                    generator(Array2Tensor(np.random.randn(gen_j,code_size)),step,alpha)
                )
            #images = generator(Array2Tensor(np.random.randn(gen_j,code_size)),step,alpha)
            #保存图片
            save_image(
                images,
                f'sample/{str(i + 1).zfill(6)}.png',
                nrow = gen_i,
                normalize=True,
                value_range=(-1,1),
            )
            mox.file.copy_parallel('./sample',args.train_url)
        #10000代储存一个生成器模型
        if (i+1) % 5000 == 0:
            ms.save_checkpoint(generator,f'checkpoint/gen{str(i + 1).zfill(6)}.ckpt')
            ms.save_checkpoint(discriminator,f'checkpoint/dis{str(i + 1).zfill(6)}.ckpt')
            mox.file.copy_parallel(src_url=ckpt_path,dst_url=ckpt_obs_path)
        
        state_msg = (
            f'Size: {4 * 2 ** step}; G: {gen_loss_value.mean()}; D: {disc_loss_value.mean()};'
            f'Alpha: {alpha}'
        )                                                               #状态信息
        
        pbar.set_description(state_msg)  



if __name__ == '__main__':
    code_size = 512
    batch_size = 32
    latent_code = Tensor(np.random.randn(batch_size,code_size),dtype=ms.float32)
    n_critic = 1
    #epoch_num = 70000 
    num_parallen_workers = 1
    dataset_sink_mode = False

    
    
    #dataset_0 = MultiResolutionDataset(local_data_path) 
    #dataset = create_dataset(dataset_0,batch_size,transform=transform)
    
    ckpt_path = 'checkpoint'
    sample_path = 'sample'
    ckpt_obs_path = 'obs://exampleyan/ms_GAN/output/ckpt/'
    ckpt_local_path = 'ckpt_to_load'
    os.mkdir(ckpt_path)
    os.mkdir(sample_path)
    os.mkdir(ckpt_local_path)

    mox.file.copy_parallel(ckpt_obs_path,ckpt_local_path)

    parser = argparse.ArgumentParser(description='Progressive Growing of GANs')

    #parser.add_argument('path', type=str, help='path of specified dataset')
    parser.add_argument('--data_url', required=True,type=str,default=None,help='location of specified dataset')
    parser.add_argument('--train_url',required=True,default=None,help='Location of training output')
    parser.add_argument(
        '--phase',
        type=int,
        default=600_000,
        help='number of samples used for each training phases',
    )
    parser.add_argument('--lr', default=0.001, type=float, help='the learning_rate dynamic')
    parser.add_argument('--sched',default=True, action='store_true', help='use lr scheduling')
    parser.add_argument('--init_size', default=8, type=int, help='initial image size')
    parser.add_argument('--max_size', default=1024, type=int, help='max image size')
    parser.add_argument(
        '--ckpt', default='ckpt_to_load', type=str, help='load from previous checkpoints'
    )
    parser.add_argument(
        '--no_from_rgb_activate',
        action='store_true',
        help='use activate in from_rgb (original implementation)',
    )
    parser.add_argument(
        '--mixing',default = False, action='store_true', help='use mixing regularization'
    )
    parser.add_argument(
        '--loss',
        type=str,
        default='wgan-gp',
        choices=['wgan-gp', 'r1'],
        help='class of gan loss',
    )

    args = parser.parse_args()

    generator = StyleGenerator(code_size)
    generator.set_train()
    discriminator = Discriminator()
    discriminator.set_train()
    disc_with_loss = DisWithLossCell(generator,discriminator)
    gen_with_loss = GenWithLossCell(generator,discriminator)
    #g_running = StyleGenerator(code_size)
    #requires_grad(g_running,False)

    g_optimizer = nn.Adam(
        generator.trainable_params(),learning_rate=0.001, beta1=1e-4,beta2=0.99
    )
    '''g_optimizer.add_param_group(
        {
            'params': generator.module.style.parameters(),
            'lr': args.lr * 0.01,
            'mult': 0.01,
        }
    )'''
    optimizer_param_groups = {
            #'params': generator.parameters(),
            'lr': args.lr ,
            'mult': 0.01,
        }
    d_optimizer = nn.Adam(discriminator.trainable_params(),learning_rate=0.001, beta1=1e-4, beta2=0.99)

    #accumulate(g_running, generator,0)

    train_one_stepD = TrainOneStepD(disc_with_loss,d_optimizer)
    train_one_stepG = TrainOneStepG(gen_with_loss,g_optimizer)
        
    if args.ckpt is not None:
        load_ckpt('ckpt_to_load/gen015000.ckpt',generator)
        load_ckpt('ckpt_to_load/dis015000.ckpt',discriminator)
        #load_ckpt(ckpt['g_running'],g_running)
        #g_optimizer.load_state_dict(ckpt['g_optimizer'])
        #d_optimizer.load_state_dict(ckpt['d_optimizer'])

    transform = Compose(                                                 #Composes several transforms together.
        [   PV.ToPIL(),
            PV.RandomHorizontalFlip(),
            PV.ToTensor(),
            PV.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
        ]
    )
    
    local_data_path = 'cache/dataset'
    mox.file.copy_parallel(src_url=args.data_url,dst_url=local_data_path)
    

    if args.sched:
        args.lr = {128: 0.0015, 256: 0.002, 512: 0.003, 1024: 0.003}
        args.batch = {4: 512, 8: 256, 16: 128, 32: 64, 64: 32, 128: 32, 256: 32}

    else:
        args.lr = {}
        args.batch = {}

    args.gen_sample = {512: (8, 4), 1024: (4, 2)}

    args.batch_default = 32

    train(args, generator, discriminator)

            


            